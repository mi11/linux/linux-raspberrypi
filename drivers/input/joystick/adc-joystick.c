// SPDX-License-Identifier: GPL-2.0
/*
 * Input driver for joysticks connected over ADC.
 * Copyright (c) 2019-2020 Artur Rojek <contact@artur-rojek.eu>
 */
/*
 * Modified by Guillaume Sanahuja to use polling instead of callbacks
*/
#include <linux/ctype.h>
#include <linux/input.h>
#include <linux/input-polldev.h>
#include <linux/iio/iio.h>
#include <linux/iio/consumer.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/property.h>

#include <asm/unaligned.h>

struct adc_joystick_axis {
	u32 code;
	s32 range[2];
	s32 fuzz;
	s32 flat;
};

struct adc_joystick {
	struct input_dev *input;
	struct adc_joystick_axis *axes;
	struct iio_channel *chans;
	int num_chans;
};

static void adc_joystick_poll(struct input_polled_dev *dev)
{
	struct adc_joystick *joy = dev->private;
	int val, i;

	for (i = 0; i < joy->num_chans; ++i) {
        iio_read_channel_processed(&joy->chans[i], &val);
		input_report_abs(joy->input, joy->axes[i].code, val);
	}

	input_sync(joy->input);
}

static int adc_joystick_set_axes(struct device *dev, struct adc_joystick *joy)
{
	struct adc_joystick_axis *axes;
	struct fwnode_handle *child;
	int num_axes, error, i;

	num_axes = device_get_child_node_count(dev);
	if (!num_axes) {
		dev_err(dev, "Unable to find child nodes\n");
		return -EINVAL;
	}

	if (num_axes != joy->num_chans) {
		dev_err(dev, "Got %d child nodes for %d channels\n",
			num_axes, joy->num_chans);
		return -EINVAL;
	}

	axes = devm_kmalloc_array(dev, num_axes, sizeof(*axes), GFP_KERNEL);
	if (!axes)
		return -ENOMEM;

	device_for_each_child_node(dev, child) {
		error = fwnode_property_read_u32(child, "reg", &i);
		if (error) {
			dev_err(dev, "reg invalid or missing\n");
			goto err_fwnode_put;
		}

		if (i >= num_axes) {
			error = -EINVAL;
			dev_err(dev, "No matching axis for reg %d\n", i);
			goto err_fwnode_put;
		}

		error = fwnode_property_read_u32(child, "linux,code",
						 &axes[i].code);
		if (error) {
			dev_err(dev, "linux,code invalid or missing\n");
			goto err_fwnode_put;
		}

		error = fwnode_property_read_u32_array(child, "abs-range",
						       axes[i].range, 2);
		if (error) {
			dev_err(dev, "abs-range invalid or missing\n");
			goto err_fwnode_put;
		}

		fwnode_property_read_u32(child, "abs-fuzz", &axes[i].fuzz);
		fwnode_property_read_u32(child, "abs-flat", &axes[i].flat);

		input_set_abs_params(joy->input, axes[i].code,
				     axes[i].range[0], axes[i].range[1],
				     axes[i].fuzz, axes[i].flat);
		input_set_capability(joy->input, EV_ABS, axes[i].code);
	}

	joy->axes = axes;

	return 0;

err_fwnode_put:
	fwnode_handle_put(child);
	return error;
}

static int adc_joystick_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct adc_joystick *joy;
    struct input_polled_dev *poll_dev;
	struct input_dev *input;
	int error;
	int i,value;

	joy = devm_kzalloc(dev, sizeof(*joy), GFP_KERNEL);
	if (!joy)
		return -ENOMEM;

	joy->chans = devm_iio_channel_get_all(dev);
	if (IS_ERR(joy->chans)) {
		error = PTR_ERR(joy->chans);
		if (error != -EPROBE_DEFER)
			dev_err(dev, "Unable to get IIO channels");
		return error;
	}

	/* Count how many channels we got. NULL terminated. */
	for (i = 0; joy->chans[i].indio_dev; i++) ;
	joy->num_chans = i;
    
    poll_dev = devm_input_allocate_polled_device(dev);
	if (!poll_dev) {
		dev_err(dev, "failed to allocate input device\n");
		return -ENOMEM;
	}

	if (!device_property_read_u32(dev, "poll-interval", &value))
		poll_dev->poll_interval = value;

    poll_dev->poll = adc_joystick_poll;
    poll_dev->private = joy;
	input = poll_dev->input;

	joy->input = input;
	input->name = pdev->name;
	input->id.bustype = BUS_HOST;

	error = adc_joystick_set_axes(dev, joy);
	if (error)
		return error;

	input_set_drvdata(input, joy);
    error = input_register_polled_device(poll_dev);
	if (error) {
		dev_err(dev, "Unable to register input device\n");
		return error;
	}

	return 0;
}

static const struct of_device_id adc_joystick_of_match[] = {
	{ .compatible = "adc-joystick", },
	{ }
};
MODULE_DEVICE_TABLE(of, adc_joystick_of_match);

static struct platform_driver adc_joystick_driver = {
	.driver = {
		.name = "adc-joystick",
		.of_match_table = adc_joystick_of_match,
	},
	.probe = adc_joystick_probe,
};
module_platform_driver(adc_joystick_driver);

MODULE_DESCRIPTION("Input driver for joysticks connected over ADC");
MODULE_AUTHOR("Artur Rojek <contact@artur-rojek.eu>, Guillaume Sanahuja <gsanahuj@hds.utc.fr>");
MODULE_LICENSE("GPL");
